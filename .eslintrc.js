module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
     //当内置的eslint规则不符合我们的要求时，可以在rule里边覆盖关闭
     "vue/multi-word-component-names": "off",
   //是用于检测当前的组件名称是否使用驼峰或多单词命名，
   //eslint默认是要求检测，可以手动关闭检测

     eqeqeq:'error',//这是全等不是三个就报错定义
     //eslintrc报错可以自定义
     "vue/no-mutating-props": "off"
  }
}
